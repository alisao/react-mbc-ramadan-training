# React , React Native , Redux and Modular CSS MBC Ramadan 2018 Training

### Training syllabus:
-   **Introduction to ES6:**   [Recommended prerequisite training](https://www.linkedin.com/learning/learning-ecmascript-6?u=2105002)
     -  Let [Closure Problem desmonstration | Codepen](https://codepen.io/ali-sao/pen/WJPwro)
     -  Const
     -  Arrow functions
     -  Imports and Exports
     -  Classes
     -  destructuring 
     -  object spreading 
     -  template literals.
     
-   **Introduction to functional programming.**
-   **React Fundamentals :**
    -   Understanding React Components.
    -   What is JSX?
    -   Working with CSS.
    -   Passing data with props.
    -   Functional VS state-full Components.
    -   Lifecycle Hooks.
    -   Events, Refs and this Binding.
    -   Understanding State.
    -   Animating React Components.
    -   Introduction to React Router.
    
-   **React Native fundamentals :**
    -   Intro, how react native was made as a new extension.
    -   Utilities ,helper functions and platform specific code
    -   Components
    -   Styling.
    -   Navigation , introduction to react navigation
    -   Animation
    -   Debugging.
-   **Redux :**
    -   Why Redux , what problem is it solving ?
    -   Working with Redux store, creating store, subscribing, listening and dispatching actions.
    -   All about actions
    -   All about reducers
    -   Understanding Reducers and dispatches
    -   Project state inside a component, displaying and updating state.
    -   Reducers compositions
    -   Working with Redux devtool
-   **Extra Topics :**
    -   Production ready applications
    -   Enhancing experience
    -   Better optimized applications.
    -   Helper and Utility Functions.


React native drawer, configurable to achieve material design style, slack style, parallax, and more. Works in both iOS and Android.

[![PRs](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://bitbucket.org/alisao/react-mbc-ramadan-training/src/master/PR.md)

- [Requirements](#Requirements)
- [Installation](#installation)
- [Running the app](#running-the-app)
- [Training Folders](#training)
- [Project Componentization illustration](#componentization)

### Requirements :
[![Node latest](https://img.shields.io/node/v/passport/latest.svg)](https://nodejs.org/en/)

### Installation
`cd <To lesson folder> && npm install`

### Running the app
`cd <To lesson folder> && npm start`

### Training Folders
-   Each lesson has its own folder , md file inside for extra notes during the session.
-   Training folders starts on May 21 2018.

### Project Componentization illustration
![MBC Ramadan training project UI](https://bitbucket.org/alisao/react-mbc-ramadan-training/raw/master/componentization.png)

