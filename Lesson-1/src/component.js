import React, { Component } from 'react';


class FirstComponent extends Component {
    constructor() {
        super();
        this.state = {
            title: 'componentation'
        }
    }
    render() {
        return (
            <div>
                <h1 className="header">{`this is my first ${this.state.title}`}</h1>
                <p>{this.props.paragText}</p>
            </div>
        );
    }
}

export default FirstComponent;
