import React from 'react';
import ReactDOM from 'react-dom';
import FirstComponent from './component';

ReactDOM.render(
    <FirstComponent />
    ,
    document.getElementById('app')
);
module.hot.accept();
