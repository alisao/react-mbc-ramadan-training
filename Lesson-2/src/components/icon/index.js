import React, { Component } from 'React';

export default class Icon extends Component {
    constructor() {
        super();
        this.state = {
            placeholder: 'http://via.placeholder.com/70x70',
            isLoading: true,
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: false
        })
    }
    render() {
        return (
            <img src={this.state.isLoading ? this.state.placeholder : this.props.src}
                height={this.props.height}
                width={this.props.width}
            />)
    }
}

