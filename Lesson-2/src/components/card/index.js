import React, { Component } from 'React';
import Icon from '../icon';

export default class Card extends Component {
    render() {
        return (
            <div className="card-container">
                <img src={this.props.src} width={this.props.width} height={this.props.height} />
                <Icon src="./assets/images/icons/clips.png" width="70" />
            </div>
        );
    }
}