import React, { Component } from 'react';

export default class Timing extends Component {
    render() {
        return (
            <img src={this.props.src} width={this.props.width} height={this.props.height} />
        );
    }
}