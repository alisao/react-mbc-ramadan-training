import React from 'react';

export default class Tab extends React.Component {
    render() {
        const { link, imgStyle, src, title } = this.props;

        return (
            <a href={link}>
                <img src={src} style={imgStyle} />
                <span className="tabTitle">{title}</span>
            </a>
        );
    }
}