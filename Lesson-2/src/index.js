import React from 'react';
import ReactDOM from 'react-dom';
import Icon from './components/icon';
import Card from './components/card';
import Tab from './components/tab';
import Timing from './components/timing';

ReactDOM.render(
    <div className="wrapper">
        <Icon src="./assets/images/icons/clips.png" width="70" />
        <Tab src="./assets/images/icons/clips.png" imgStyle={{ width: '100px', height: '60px' }} link='#' title="Spider Man" />
        <Card src="https://picsum.photos/190/140?image=21" width="190" height="140" />
        <Timing src="./assets/Mock/spiderman/timing.png" width="70" />
    </div>
    ,
    document.getElementById('app')
);

module.hot.accept();
