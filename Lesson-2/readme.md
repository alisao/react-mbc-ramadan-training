## Topics Discussed 
- **React Project Structure**
- **Lifecycle hooks , fetching data in render function**
    - General usage :
    ```
        fetch()
        .then(result=>result.json('<END POINT>'))
        .then(data=>do something)
    ```
    - How and where to fetch :
        1. Who is interested in this data? 
            The fetching component should be a common parent component for all these components.
        2. Where do you want to show a conditional loading indicator 
            (e.g. loading spinner, progress bar) when the fetched data from the asynchronous request is pending? 
            The loading indicator could be shown in the common parent component from the first criteria. 
            Then the common parent component would still be the component to fetch the data.
        2.1. But when the loading indicator should be shown in a more top level component, 
            the data fetching needs to be lifted up to this component.
        2.2. When the loading indicator should be shown in child components of the common parent component, 
            not necessarily the components that need the data, the common parent component would still be the component to fetch the data. 
            The loading indicator state could then be passed down to all child components that would be interested to show a loading indicator.

        3. Where do you want to show an optional error message when the request fails? Here the same rules from the second criteria apply.

- **Events , refs and this binding** (incompleted)
- **Understanding state**
- **Animation**
    - Animations with pure function .
    - As easy as toggeling css classes on/off
    
        ```
            const ExampleComponent = ({show}=this.props) => {
                const componentClasses = ['example-component'];
                if (show) { componentClasses.push('show'); }
                return (
                <div className={componentClasses.join(' ')}></div>
                );
            };
            .example-component {
                width: 100px;
                height: 100px;
                background-color: red;
                -webkit-transition: 0.5s;
                -moz-transition: 0.5s;
                -o-transition: 0.5s;
                transition: 0.5s;
                opacity: 0;
                visibility: hidden; 
            }
            .example-component.show {
                opacity: 1;
                visibility: visible; 
            }
        ```

- **React Router introduction**
    - React Router V4 [documentation](https://reacttraining.com/react-router/web/guides/philosophy) 
    - [CodePen example](https://codepen.io/ali-sao/full/ZoPwja)
    - Installation 
        ```sh
        npm install --save react-router-dom
         ```
    - Major components :
        - Router : <BrowserRouter> - Server redering routes || <HashRouter> - Static rendering websites
        - Route : <Route component={<COMPONENT NAME>} path={PATH}>
        - Link : used instead of href for internal linking  <Link to='/'>Home</Link>
        - Use the tester to know more about match object: https://pshrmn.github.io/route-tester/